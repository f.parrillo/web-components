import { Observable } from '../../node_modules/rxjs-compat/Observable';

export interface View<Model> {
   update(model:Model): void
   addObservable(model: Observable<Model>): void
}

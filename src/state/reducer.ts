export interface Reducer<Model, Action> {
    action: Action
    reaction(model: Model): Model
}

import { Reducer } from './reducer';
import { BehaviorSubject } from '../../node_modules/rxjs/internal/BehaviorSubject';

export interface BaseState<Model, Action> {
    model: BehaviorSubject<Model>;
    reducers: Map<Action, Reducer<Model, Action>>;
}

export class BaseState<Model, Action> implements BaseState<Model, Action> {
    model: BehaviorSubject<Model>;
    reducers: Map<Action, Reducer<Model,Action>> = new Map<Action, Reducer<Model, Action>>();

    addReducer(reducer: Reducer<Model,Action>): void{ this.reducers.set(reducer.action, reducer)}

    reactTo(action: Action): void {
        const reducer = this.reducers.get(action)
        // @ts-ignore
        const updatedModel: Model = reducer.reaction(this.model.getValue())
        this.model.next(updatedModel)
    }
}


import { fromEvent } from '../node_modules/rxjs/internal/observable/fromEvent';
import { Counter } from './counter/counter';
import { CounterState } from './counter/state';
import { DecrementReducer, IncrementReducer } from './counter/reducers';


/*Events */

const onDoubleClick = (htmlElement: HTMLElement, counter: Counter, fun: (x: any) => void ): void => {
	htmlElement.addEventListener('dblclick', event => {
		fun(counter)
	});
}

const onClick = (htmlElement: HTMLElement, counter: Counter, fun: (x: any) => void ): void => {
	htmlElement.addEventListener('click', event => {
		console.log("click")
		fun(counter)
	});
}

type ids = 'button-increment' | 'button-decrement' | 'my-counter'

const template = `<div>
	<my-counter id="my-counter"></my-counter>
	<button id="button-increment">Increment</button>
	<button id="button-decrement">Decrement</button>	
</div>`

interface MyComponentElement extends HTMLElement {
	getElementById(id: ids): HTMLElement
}
class MyComponent extends HTMLElement implements MyComponentElement
	{
		private counterState: CounterState = new CounterState({counter: 0});

		constructor()
		{
			super();

			const shadow = this.attachShadow( { mode: 'open' } );

			this.counterState.addReducer(new DecrementReducer())
			this.counterState.addReducer(new IncrementReducer())

			shadow.innerHTML = template

			const counter = this.getElementById('my-counter') as Counter
			counter.addObservable(this.counterState.model.asObservable())

			const buttonIncrement = this.getElementById('button-increment')
			const buttonDecrement = this.getElementById('button-decrement')
			fromEvent(buttonDecrement, 'click')
				.subscribe(c => {
					this.counterState.reactTo('decrement')
					console.log("Decremented")
				})
			fromEvent(buttonIncrement, 'click')
				.subscribe(c => {
					this.counterState.reactTo('increment')
					console.log("Increment")
				})

			this.counterState.model.asObservable().subscribe(s => console.log("It should work"))
		}

		 getElementById(id: ids): HTMLElement {
			// @ts-ignore
			return this.shadowRoot.getElementById(id)
		}
	};



export const WebComponent = (tagName: string = "my-component"): {
	defineWebComponent: () => void
} => {
	console.log(tagName)
	return {
		defineWebComponent: () => {
			if ( customElements.get( tagName ) ) { return; }
			customElements.define( tagName, MyComponent )
			console.log(customElements.get(tagName))
		}
	}
}

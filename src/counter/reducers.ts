import { CounterModel } from './model';
import { Reducer } from '../state/reducer';

type IncrementAction = 'increment'

export class IncrementReducer implements Reducer<CounterModel, IncrementAction >{
    action: IncrementAction  = 'increment';
    reaction(model: CounterModel): CounterModel {
        console.log("Increment " + model.counter)
        return  {...model, counter: model.counter + 1}
    }
}

type DecrementAction = 'decrement'
export class DecrementReducer implements Reducer<CounterModel, DecrementAction>{
    action: DecrementAction  = 'decrement';
    reaction(model: CounterModel): CounterModel {
        return  {...model, counter: model.counter - 1}
    }
}

export type  Action = DecrementAction | IncrementAction

import { CounterModel } from './model';
import { Action } from './reducers';
import { BaseState } from '../state/base-state';
import { BehaviorSubject } from '../../node_modules/rxjs/internal/BehaviorSubject';

export class CounterState extends BaseState<CounterModel, Action> {
    constructor(model: CounterModel){
        super()
        this.model = new BehaviorSubject<CounterModel>(model)
    }
}

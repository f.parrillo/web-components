import { CounterModel } from './model';
import { View } from '../state/view';
import { Observable } from '../../node_modules/rxjs-compat/Observable';

export class Counter extends HTMLElement implements View<CounterModel>{
	private readonly div: HTMLDivElement;
		constructor()
		{
			super()
			this.attachShadow( { mode: 'open' } );
			this.div = document.createElement('div')
			// @ts-ignore
			this.shadowRoot.appendChild(this.div)
		}

		update(model: CounterModel) {
			this.div.innerHTML = model.counter.toString()
		}

	addObservable(model: Observable<CounterModel>): void {
			model.subscribe((model: CounterModel) => this.update(model))
	}
	}





export const WebComponent1 = (tagName: string = "my-counter"): {

	defineWebComponent: () => void
} => {
	console.log(tagName)
	return {
		defineWebComponent: () => {
			if ( customElements.get( tagName ) ) { return; }
			customElements.define( tagName, Counter )
			console.log(customElements.get(tagName))
		}
	}
}
